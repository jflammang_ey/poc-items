({
    doInit : function(component, event, helper) {
        helper.mockTableData(component, event, helper);
    },
    
    handleRowSelection : function(component,event,helper){

    },
    
    handleButtonClick : function(component,event,helper){
        const button = event.getSource();
        const value = button.get('v.value');
        
        switch(value) {
            case 'save_and_close_modal':
                helper.addHouseMember(component,event,helper);
                component.set('v.showModalDiv',false);
                break;
            case 'close_modal':
                component.set('v.showModalDiv',false);
                break;
            case 'open_modal':
                component.set('v.showModalDiv',true);
                break;
        }
    },
    
})