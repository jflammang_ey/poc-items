({
    
    mockTableData : function(component,event,helper){

        let houseMemberList = [
            {
                Id:"1",
                fName:"House",
                mName:"",
                lName:"Contact1",
                age: 50,
                dob: new Date(1971,11,2),
                phone: '123-123-1234',
                dateLastSeen: new Date(2021,2,12),
            },
        ];
        component.set('v.houseMemberList',houseMemberList);    
        
        let actions = [
            { label: 'Edit', name: 'edit' },
            { label: 'Delete', name: 'delete' }
        ];

        let hmColumns = [
            {label: 'First Name', fieldName: 'fName', type: 'text'},
            {label: 'Middle Name', fieldName: 'mName', type: 'text'},
            {label: 'Last Name', fieldName: 'lName', type: 'text'},
            {label: 'Age', fieldName: 'age', type: 'number', cellAttributes: { alignment: 'left' }},
            {label: 'DOB', fieldName: 'dob', type: 'date', typeAttributes: { year:'numeric', month:'long', day:'numeric'} },
            {label: 'Phone', fieldName: 'phone', type: 'text'},
            {label: 'Date Last Seen', fieldName: 'dateLastSeen', type: 'date', typeAttributes: { year:'numeric', month:'long', day:'numeric'} },
            { type: 'action', typeAttributes: { rowActions: actions } }
        ];
        component.set('v.hmColumns',hmColumns);
    },

    addHouseMember : function(component,event,helper){
        const members = component.get('v.houseMemberList');
        let houseMembers = component.get('v.houseMembers');
        houseMembers = $A.util.isUndefinedOrNull(houseMembers) ? [] : houseMembers;
        houseMembers.push(members[0]);
        component.set('v.houseMembers',houseMembers);  
    },

})