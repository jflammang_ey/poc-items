({
    doInit : function(component, event, helper) {
        let addressObj = {};
        addressObj.administrative_area_level_1 = '';
        addressObj.administrative_area_level_2 = '';
        addressObj.country = '';
        addressObj.formattedAddress = '';
        addressObj.fullStreetAddress = '';
        addressObj.latitude = '';
        addressObj.locality = '';
        addressObj.longitude = '';
        addressObj.postal_code = '';
        addressObj.premise = '';
        addressObj.route = '';
        addressObj.street_number = '';
        addressObj.placeId ='';
        component.set('v.addressObj',addressObj);
        component.set('v.addressObjStr',JSON.stringify(addressObj, null, 2));
        
        const unitValid = component.get('v.unitValid');
        const unitRequired = component.get('v.unitRequired');
        let unitMessage;
        if(unitRequired){
            if(unitValid){
                unitMessage = 'Validated';
            } else {
                unitMessage = 'Invalid Unit Number';
            }
        } else {
            unitMessage = 'Not Required';
            component.set('v.unitValid',true);
        }
        component.set('v.unitMessage',unitMessage);
        
        // Set the validate attribute to a function that includes validation logic
        component.set('v.validate', function() {
            const addressValid = component.get('v.addressValid');
            if(addressValid) {
                // If the component is valid...
                return { isValid: true };
            }
            else {
                // If the component is invalid...
                return { isValid: false, errorMessage: 'An address is required.' };
            }})
	},
    
	handleChange : function(component, event, helper) {
		console.log('Address Changed');
        component.set('v.status','I just CHANGED!!');
        const address = component.get('v.address');
        const input = component.find('unitInput');
        component.set('v.address',address);
        const unit = component.get('v.unit');
        // For testing POC
        if(unit == 'A'){
            component.set('v.unitValid',true);
            component.set('v.unitMessage','Validated');
            input.setCustomValidity('');
        } else {
            component.set('v.unitValid',false);
            component.set('v.unitMessage','Invalid Unit Number');
            input.setCustomValidity('This does not appear to be a valid unit number for this address.');
        }
	},
    
    handleClick : function(component, event, helper) {
        const address = component.get('v.address');
        const input = component.find('unitInput');
        component.set('v.address',address);
        const unit = component.get('v.unit');
        if(unit == 'A'){
            component.set('v.unitValid',true);
            input.setCustomValidity('');
        } else {
            component.set('v.unitValid',false);
            input.setCustomValidity('This does not appear to be a valid unit number for this address.');
        }
    },
    
    handleChangeAddress : function(component, event, helper) {
        const addressObj = component.get('v.addressObj');
        const formattedAdd = $A.util.isEmpty(addressObj.formattedAddress) ? 'Not Set' : addressObj.formattedAddress;
        console.log('addressObj changed!: ',JSON.stringify(addressObj));
        component.set('v.addressObjStr',JSON.stringify(addressObj, null, 2));
        component.set('v.address',formattedAdd);
    }
    
})