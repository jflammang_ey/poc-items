({
    doInit : function(component, event, helper) {


    },
    
    updateFlowPath : function(component, event, helper) {
        const flowPath = component.find('flowPath');
        flowPath.updateCurrentStage();
    },
    
    handleStatusEvent: function(component, event, helper) {
          var currentStageIndex = component.get("v.currentStageIndex");
          component.set("v.currentStageIndex",currentStageIndex+1);
          updateFlowPath(component, event, helper);
          
    },

    handleComponentEvent : function(component, event, helper) {
        console.log('\n vvvvvv: ',JSON.stringify(event.getSource()));
        alert("\n 11111      in parent handleComponentEvent");
        var flowEventMessage = event.getParam("message");
        alert(flowEventMessage);

    },

    handleStageSelected : function(component, event, helper){
        const selectedStageIndex = event.getParam('stageIndex');
        component.set('v.activeStageIndex',selectedStageIndex);
        console.log('Selected Stage Index: ',selectedStageIndex);
    },
})