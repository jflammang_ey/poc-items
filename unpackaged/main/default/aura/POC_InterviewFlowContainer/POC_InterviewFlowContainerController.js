({
    doInit : function(component, event, helper) {
        const flow = component.find("flowData");
        flow.startFlow("Sample_Flow_for_LC");

        // test
        //const guid = '364506b9484b5a4569c620bff5f617845786568-1ed1';
        //flow.resumeFlow(guid);
    },

    statusChange : function (component, event, helper) {
        const activeStages = event.getParam('activeStages');
        const currentStage = event.getParam('currentStage');
        const flowTitle = event.getParam('flowTitle');
        const helpText = event.getParam('helpText');
        const guid = event.getParam('guid');
        const outputVariables = event.getParam('outputVariables');
        const status = event.getParam('status');

        console.log('activeStages: ',JSON.stringify(activeStages));
        console.log('currentStage: ',JSON.stringify(currentStage));
        console.log('flowTitle: ',JSON.stringify(flowTitle));
        console.log('helpText: ',JSON.stringify(helpText));
        console.log('guid: ',JSON.stringify(guid));
        console.log('outputVariables: ',JSON.stringify(outputVariables));
        console.log('status: ',JSON.stringify(status));
        
    },
})