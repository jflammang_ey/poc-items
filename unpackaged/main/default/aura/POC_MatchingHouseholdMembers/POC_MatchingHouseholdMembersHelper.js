({
    
    mockTableData : function(component,event,helper){
        let data = [
            {
                Id:"1",
                fName:"Sample",
                lName:"Case1",
                age: 50,
                dob: new Date(1971,11,2),
                language: 'English',
                type: 'Case',
                status: "Ongoing Follow-up",
                matchType: "Home/Unit Address",
            },
            {
                Id:"2",
                fName:"Possible",
                lName:"Contact1",
                age: 45,
                dob: new Date(1976,9,14),
                language: 'English',
                type: 'Possible Contact',
                status: "New",
                matchType: "Building Address",
            },
            {
                Id:"3",
                fName:"Possible",
                lName:"Contact2",
                age: 23,
                dob: new Date(1998,4,22),
                language: 'English',
                type: 'Possible Contact',
                status: "Ongoing Follow-up",
                matchType: "Building Address",
            },
            {
                Id:"4",
                fName:"Possible",
                lName:"Contact3",
                age: 34,
                dob: new Date(1987,5,12),
                language: 'Spanish',
                type: 'Possible Contact',
                status: "New",
                matchType: "Building Address",
            },
            {
                Id:"5",
                fName:"Possible",
                lName:"Contact4",
                age: 21,
                dob: new Date(2000,8,18),
                language: 'English',
                type: 'Possible Contact',
                status: "New",
                matchType: "Building Address",
            },
            {
                Id:"6",
                fName:"Sample",
                lName:"Case2",
                age: 61,
                dob: new Date(1960,4,9),
                language: 'English',
                type: 'Case',
                status: "Initial Interview Completed",
                matchType: "Building Address",
            },
            {
                Id:"7",
                fName:"Possible",
                lName:"Contact5",
                age: 21,
                dob: new Date(2000,8,18),
                language: 'English',
                type: 'Possible Contact',
                status: "New",
                matchType: "Building Address",
            },
        ];
        component.set('v.data',data);    
        
        let columns = [
            {label: 'Type', fieldName: 'type', type: 'text'},
            {label: 'First Name', fieldName: 'fName', type: 'text'},
            {label: 'Last Name', fieldName: 'lName', type: 'text'},
            {label: 'Age', fieldName: 'age', type: 'number', cellAttributes: { alignment: 'left' }},
            {label: 'DOB', fieldName: 'dob', type: 'date', typeAttributes: { year:'numeric', month:'long', day:'numeric'} },
            {label: 'Language', fieldName: 'language', type: 'text'},
            {label: 'Status', fieldName: 'status', type: 'text'},
            {label: 'Match Type', fieldName: 'matchType', type: 'text'}
        ];
        component.set('v.columns',columns);
    },

})