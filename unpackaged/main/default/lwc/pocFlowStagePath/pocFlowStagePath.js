import { LightningElement, api, track } from 'lwc';

export default class PocFlowStagePath extends LightningElement {
    @api activeStageIndex = 0;
    coachingExpanded = false;
    stageStatus;
    stageModBy;
    stageModDate;
    @api recordId;
    @api currentStageIndex = 0;
    //  TODO: These stages should be defined in records/custom metadata.  The data will drive the name of the stage, the order in which it should appear in the interview, and the API name of the flow to be rendered for that stage.
    //  This JSON could then be saved to the Case/PC record, and updated with property values specific to the record: stage status, stage modified date/user, etc.
    @track stages = [
        {Id: 1, stageName: 'Intro', status: 'In Progress', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 2, stageName: 'Consent & Verify', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Susan Thomas", lastModDate: 1547250828000 },
        {Id: 3, stageName: 'Phone & Address', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Mary Leman", lastModDate: 1547250828000 },
        {Id: 4, stageName: 'Demographic Info', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 5, stageName: 'Exposure Source', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 6, stageName: 'High Risk Exposure', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 7, stageName: 'Employment Info', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 8, stageName: 'Current Health Status', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 9, stageName: 'Household Contacts', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 10, stageName: 'Site of Transmission', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 11, stageName: 'Follow Up', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 12, stageName: 'MD Covid Alert', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
        {Id: 13, stageName: 'Wrap Up', status: 'Not Started', isComplete: false, isActive: false, isCurrent: false, isDisabled: false, lastModBy: "Jane Doe", lastModDate: 1547250828000 },
    ];

    connectedCallback() {
        this.setStageProperties();
    }

    handleStageClick(event){
        const stageId = event.currentTarget.getAttribute("data-stageid");
        //console.log('Stage Clicked (DOM Id): ',event.currentTarget.id);
        //console.log('Stage Clicked (Stage Id): ',stageId);
        const stageIndex = this.stages.findIndex(function(stage){ return stage.Id == stageId });
        if(stageIndex === -1){
            return;
        }
        if(this.stages[stageIndex].isDisabled){
            return;
        }
        //console.log('Stage Index: ',stageIndex);
        this.activeStageIndex = stageIndex;
        this.setStageProperties();
        const selectedEvent = new CustomEvent('stageselected', { detail: {stageIndex} });
        this.dispatchEvent(selectedEvent);
    }

    toggleCoaching(event){
        const isExpanded = !this.coachingExpanded;
        const pathDiv = this.template.querySelector('div.slds-path');
        const toggleBtn = this.template.querySelector('lightning-button-icon[data-id=detailsToggle]');
        const pathContent = this.template.querySelector('div.slds-path__content');
        console.log('toggleBtn: ',toggleBtn);
        console.log('toggleBtn.className: ',toggleBtn.className);

        this.coachingExpanded = isExpanded;

        if(isExpanded){
            pathDiv.classList.add('slds-is-expanded');
            pathContent.classList.remove('slds-is-collapsed');
            toggleBtn.className = 'slds-path__trigger slds-path__trigger_open';
        } else {
            pathDiv.classList.remove('slds-is-expanded');
            pathContent.classList.add('slds-is-collapsed');
            toggleBtn.className = 'slds-path__trigger';
        }
    }

    @api 
    updateCurrentStage(){
        this.activeStageIndex = this.currentStageIndex;
        this.setStageProperties();
    }

    setStageProperties(){
        console.log('Updating stage properties');
        const stageLength = this.stages.length;
        const requiredSectionIndex = 1; //  This is to mock requiring certain sections before the interviewer can try to navigate ahead to subsequent sections, i.e. the Consent & Verify section
        const requiredSectionCompleted = this.currentStageIndex > requiredSectionIndex;
        //  Default the mod by and date to null.
        this.stageModBy = null;
        this.stageModDate = null;

        for(let i=0; i< stageLength; i++){
            const mockPausedStage = i == 2;
            const stageActive = i == this.activeStageIndex;
            //  Check if stage is the current stage selected OR stage 2 (hard coded as a paused section for POC)
            const stageCurrent = (i == this.currentStageIndex);
            const stageComplete = (i < this.currentStageIndex) && !mockPausedStage;
            const isDisabled = i > requiredSectionIndex && !requiredSectionCompleted;
            let liClass = 'slds-path__item';
            this.stages[i].isActive = stageActive;
            this.stages[i].isCurrent = stageCurrent;
            this.stages[i].isComplete = stageComplete;
            this.stages[i].isDisabled = isDisabled;
            this.stages[i].aClass = 'slds-path__link';

            if(this.stages[i].isComplete){
                liClass += ' slds-is-complete';
            }
            else {
                liClass += ' slds-is-incomplete';
            }
            
            //  If a section is disabled, add custom class to apply a disabled styling
            if(isDisabled){
                this.stages[i].aClass += ' isDisabled';
            }
            //  Add mock paused styling to stage
            else if(mockPausedStage){
                liClass += ' isPaused';
            }
            if(stageActive){
                liClass += ' slds-is-active';
                this.stageStatus = this.stages[i].status;
                // TESTING: only show a last mod date/user when stage completed or current
                if(stageComplete || stageCurrent){
                    this.stageModBy = this.stages[i].lastModBy;
                    this.stageModDate = this.stages[i].lastModDate;
                }
            }
            if(stageCurrent){
                liClass += ' slds-is-current';
            }
            let status = 'Not Started';
            if(stageComplete){
                status = 'Completed';
            }
            else if(mockPausedStage){
                status = 'Paused';
            }
            else if(stageCurrent){
                status = 'In Progress';
            }
            // TODO: can add slds-is-lost to style the stage as red.  maybe use to indicate an error with the stage?
            this.stages[i].liClass = liClass;
            this.stages[i].status = status;
        }

    }
}